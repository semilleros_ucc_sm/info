---
title: MAVI
comments: false
---

# Materiales y vías

## Misión
Forjar y adecuar proyectos de investigación científica para generar conocimiento útil de los diferentes materiales, procesos y técnicas en el desarrollo de obras civiles, generando sentido investigativo entre los estudiantes del programa, con el soporte de un grupo de profesionales comprometidos con el avance de las técnicas, materiales y la calidad de estos en la ejecución de los procesos constructivos.

## Visión
Convertirse en un grupo líder de propuestas de investigación de alta calidad para la formación de excelentes investigadores que puedan tributar conocimiento de valor, demostrando la mejor calidad de formación académica e investigativa con sentido de responsabilidad ética y social.

## Líneas de investigación
Estructuras Materiales y Suelos, Vías y carreteras.


 
 
