---
title: DSE
comments: false
---

# Diseño de Sistemas Electronicos

## Misión
Generar conocimiento y cultura investigativa de calidad que fortalezca la línea de investigación en el diseño de sistemas electrónicos y produzca un valor para la comunidad académica en general y en beneficio de los estudiantes del programa de Ingeniería Electrónica de la Universidad Cooperativa de Colombia seccional Santa Marta.


## Visión
El semillero diseño de sistemas electrónicos, para el año 2017, será un grupo reconocido por la calidad de su investigación, posicionado entre los mejores semilleros de investigación a nivel departamental y nacional, ser innovadores, pioneros ante los cambios tecnológicos que sugiere el mundo actual.


## Línea de investigación
Automatización Industrial.


