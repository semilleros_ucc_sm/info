---
title: MESA
comments: false
---

# Semillero Modelación Estructural, Simulación Y Análisis

## Misión
El semillero de investigación en ingeniería estructural – MESA “MODELACION ESTRUCTURAL SIMULACIÓN Y ANÁLISIS”, fomenta la construcción del espíritu científico de los estudiantes mediante la formación crítica y reflexiva de estos, para abordar problemas de la ciencia y la sociedad relacionados con la ingeniería estructural.

## Visión
MESA será en el año 2023 un semillero de investigación consolidado que formula propuestas de investigación y desarrolla proyectos de impacto regional, nacional e internacional que aporta soluciones innovadoras a problemáticas de la ciencia y la sociedad relacionados con la Ingeniería estructural.

## Líneas de investigación
Estructuras, materiales y suelos.

 
