---
title: SIIC
comments: false
---

# Semillero de Innovación e Ingenio Computacional

## Misión 
El Semillero de Innovación e Ingenio Computacional SIIC de la Universidad Cooperativa de Colombia sede Santa Marta, tiene como misión fomentar la investigación como apoyo y solución a las diferentes problemáticas del entorno. Así como propiciar espacios de reflexión, debate y análisis sobre diferentes temas relacionados con las tecnologías de la información y las comunicaciones. Buscando despertar capacidades y aptitudes propias del trabajo de investigación.

## Visión
El semillero de investigación se proyecta dentro de su ciclo de formación las siguientes metas:

- Contribuir a desarrollar y fortalecer juntamente con los estudiantes, docentes y egresados; la investigación al interior de la universidad cooperativa de Colombia sede santa marta.

- Formar parte de una red de investigación compuesta por universidades, empresas, y asociaciones, para la formulación y ejecución de proyectos que contribuyan al desarrollo social de la región y el país.

## Líneas de investigación
Tecnologías de la Información y de las Comunicaciones

