---
title: Profesores
comments: false
---


| Profesor                             | CvLAC                                                                                                       |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------|
| Freddy Armando Cuervo Lara           | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001392491) |
| Arthur Jose Burgos Rodriguez         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000170575) |
| Anamaria De Los Angeles Franco Leyva | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001585708) |
| Arturo Ivan Villegas Andrade         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001776159) |
| Diego Andrés Restrepo Leal           | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001529337) |
| Gabriel Antonio Marquez Anaya        | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001134140) |
| Gustavo Adolfo Correa Solano         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001611825) |
| Jose Fernando Noguera Polania        | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001430899) |
| Laura Nieto Henao                    | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000004514) |
| Leandro Raul Rozo Martinez           | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001606713) |
| Luis Jorge Duran Charris             | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000684740) |
| Maria Margarita Rosa Sierra Carrillo | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000017120) |
| Oscar Hernando Moreno Torres         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000317721) |
| Sabina Edith Rada Mendoza            | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001590119) |
| Sergio Daniel Martínez Campo         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001554748) |
| Walter Camilo Tovar Coquies          | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0001431574) |
| Hernan Mario Zabarain Guerra         | [Link CvLAC](https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000085772)|
