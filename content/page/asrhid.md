---
title: ASRHID
comments: false
---

# Semillero de aprovechamiento Sostenible de los Recursos Hídricos e Hidráulicos

## Misión
El semillero del aprovechamiento sostenible de los recursos hídricos e hidráulicos (ASRHID) de la Universidad Cooperativa de Colombia sede Santa Marta busca crear espacios de profundización en la aplicación de la ingeniería hidráulica a través del trabajo investigativo. Lo anterior, se logra a través del desarrollo de proyectos investigativos que propendan a dar solución a las problemáticas relacionadas al recurso hídrico e hidráulico en la región y en el país, a la vez que se despiertan en los estudiantes las capacidades y aptitudes propias del trabajo investigativo a través de espacios de reflexión, debate y análisis.



## Visión 
El semillero de investigación se proyecta al año 2028 como un semillero de ingeniería hidráulica que será reconocido en la región Caribe de Colombia, y en el país, por sus aportes al desarrollo de la calidad de vida y la economía de la región a través de la investigación y del uso de nuevas tecnologías.



##  Línea de investigación 
Ingeniería hidráulica
