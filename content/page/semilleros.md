---
title: Semilleros
comments: false
---


| Profesor - Lider                     | Semillero                                                                                                       |
|--------------------------------------|---------------------------------------------------------------------|
| Freddy Armando Cuervo Lara           | [Movilidad Y Transporte - MOVITRANS](https://ingucc.gitlab.io/info/page/movitrans/)|
| Arturo Ivan Villegas Villegas        | [Materiales Y Vias - MAVI](https://ingucc.gitlab.io/info/page/mavi/)|
| Jose Fernando Noguera Polania        | [Diseño de Sistemas Electronicos - DSE](https://ingucc.gitlab.io/info/page/dse/)|
| Laura Nieto Henao                    | [Aprovechamiento Sostenible de Recursos Hídricos e Hidráulicos - ASRHID](https://ingucc.gitlab.io/info/page/asrhid/)|
| Leandro Raul Rozo Martinez           | GESTION DE RIESGO                                                   |
| Maria Margarita Rosa Sierra Carrillo | [Gestión De Recursos Hídricos Y Saneamiento Básico - HYDROS](https://ingucc.gitlab.io/info/page/hydros/)|
| Oscar Hernando Moreno Torres         | SUELOS Y MATERIALES                                                 |
| Sabina Edith Rada Mendoza            | [Semillero de Innovación e Ingenio Computacional - SIIC](https://ingucc.gitlab.io/info/page/siic/)|
| Hernan Mario Zabarain G              | [Ingenio, Diseño Y Cacharreo - I+D+C](https://ingucc.gitlab.io/info/page/i+d+c/)|
| Walter Camilo Tovar Coquies          | [Semillero Modelación Estructural, Simulación Y Análisis - MESA](https://ingucc.gitlab.io/info/page/mesa/)|
