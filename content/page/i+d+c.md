---
title: I+D+C
comments: false
---

# Ingenio, Diseño Y Cacharreo

## Misión
El Semillero de Investigación "I+D+C" tiene como misión fomentar la investigación y el desarrollo tecnológico en el campo de la ingeniería electrónica, mediante la formación de un equipo interdisciplinario de estudiantes y docentes, que promueva la innovación y la aplicación práctica del conocimiento adquirido en el aula de clase brindando soluciones a problemáticas regionales que aporten a la conservación de los recursos naturales y utilización de recursos energéticos renovables que minimicen el impacto sobre el ecosistema.

## Visión
El Semillero de Investigación "I+D+C" tiene como visión ser reconocido como un referente académico en el campo de la ingeniería electrónica, a nivel regional y nacional, por su compromiso con la excelencia académica y la generación de conocimiento innovador.

## Líneas de investigación
Ingeniería Electrónica, enfocada en el diseño, desarrollo e implementación de soluciones tecnológicas innovadoras.


