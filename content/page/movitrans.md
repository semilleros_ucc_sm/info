---
title: MOVITRANS
comments: false
---

# Movilidad Y Transporte

## Misión
El semillero movilidad y transporte se dedicará a investigar sobre los temas relacionados con la movilidad y el transporte en entornos urbanos y rurales, con el fin de proponer soluciones a problemáticas desde los principios de la ingeniería de tránsito.

## Visión
El semillero de movilidad y transporte será en 2025 un semillero referente en investigaciones de movilidad y transporte, haciéndose visible en la participación de eventos de semilleros y participación en proyectos de investigación del área de transporte.

## Líneas de investigación
Vías y carreteras.

