---
title: HYDROS
comments: false
---

# Gestión De Recursos Hídricos Y Saneamiento Básico

## Misión
El semillero de investigación en Gestión de Recursos Hídricos y Saneamiento Básico – HYDROS fomenta la construcción del espíritu científico de los estudiantes mediante la formación crítica y reflexiva de estos para abordar problemas de la ciencia y la sociedad relacionados con la gestión integral de los recursos hídricos superficiales y subterráneos, los procesos de tratamiento de agua potable y residuales, la captación, transporte y adaptación de modelos para la gestión de redes de distribución y el desarrollo sostenible.

## Visión
Hydros será en el año 2025 un semillero de investigación consolidado que formula propuestas de investigación y desarrolla proyectos de impacto regional, nacional e internacional que aporta soluciones innovadoras a problemáticas de la ciencia y la sociedad relacionados con la gestión integral de los recursos hídricos bajo el enfoque.

## Líneas de investigación
Energías, aguas y medio ambiente.

## Proyectos en curso

- Sistema de monitoreo en tiempo real del nivel freático y de calidad del acuífero de la ciudad de Santa Marta.
- Determinación de las concentraciones de elementos traza metálicos a partir de registros sedimentarios de lagos de alta montaña de La Sierra Nevada De Santa Marta.
- Degradación de Micro contaminantes orgánicos presentes en aguas residuales mediante procesos de Oxidación Avanzada por Fotocatálisis.
- Concreción de un vivero de especies de manglar bajo el enfoque de laboratorio vivo para la reforestación de zonas degradadas en la Ciénaga Grande De Santa Marta Municipio De Pueblo viejo- Colombia.

