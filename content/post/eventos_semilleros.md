---
title: Participación en eventos de nuestros semilleros
date: 2024-01-28
comments: false
---

# Encuentro regional Magdalena Redcolsi 2022

## Descripción del evento
La Fundación RedCOLSI organiza anualmente el Encuentro Departamental de Semilleros de Investigación, un espacio para que estudiantes y docentes presenten los avances de sus investigaciones formativas. El objetivo es promover la socialización, aprendizaje y discusión, cualificar el trabajo de los semilleros, fortalecer la colaboración en red y fomentar el intercambio de actividades de formación. Este encuentro también facilita la evaluación y cualificación de los proyectos, así como la creación de redes temáticas para contribuir al desarrollo regional. En el caso específico del XX EDESI 2022, se llevará a cabo los días 4 y 5 de agosto, reuniendo a instituciones educativas del Distrito de Santa Marta y el Departamento del Magdalena, con el propósito de compartir, socializar y debatir los proyectos de investigación de los semilleros adscritos a la Fundación RedCOLSI a través del Nodo Magdalena.

- [Link del evento](http://www.fundacionredcolsi.org/eventos/index.php?evento_id=347)

# Encuentro nacional Redcolsi 2022.

## Descripción del evento
La apuesta por la investigación formativa se convierte en una oportunidad fundamental para potenciar la ciencia. Esta versión del XXI Encuentro de Semilleros de Investigación se realizará en modalidad presencial, sujeto a las condiciones de aforo y reglamentaciones relacionadas a la realización de eventos masivos en medio de la contingencia del COVID - 19.

- [Link del evento](http://fundacionredcolsi.org/eventos/index.php?evento_id=330)


# Encuentro regional Redcolsi Guajira 2023

## Descripción del evento
La RedCOLSI es una organización sin fines de lucro que promueve la investigación en educación básica, secundaria y superior. Desde su reactivación en 2016, el Nodo Guajira ha trabajado con ocho instituciones en la generación de conocimiento científico. Con 20 Nodos Departamentales, 500 instituciones y 8,000 semilleros, la RedCOLSI ha mejorado exponencialmente desde su inicio en 1998. Organizan encuentros a nivel nacional e internacional, y el Nodo Guajira ha destacado internacionalmente desde 2017. En el XXII Encuentro Nacional en 2022, lograron 16 proyectos sobresalientes y 30 meritorios con aval internacional. Actualmente, se prepara el VI Encuentro Departamental de Semilleros de Investigación en el Nodo Guajira, con el tema "Multiculturalidad investigativa como Gestión de Conocimiento en La Guajira". Este evento busca fomentar el intercambio de aprendizajes, la discusión de la investigación y la formación de redes científicas para el desarrollo regional.

- [Link del evento](http://www.fundacionredcolsi.org/eventos/index.php?evento_id=357)
