---
title: Informe General De Actividades De Investigación
date: 2023-08-17
author: Gustavo Correa-Solano y Arturo Villegas-Andrade
draft: false
---

**Análisis Computacional De La Concentración De Esfuerzos En Muestras Birrefringentes Con Esfuerzos Residuales, Basado En Fotoelasticidad Dinámica Y Técnicas De Aprendizaje De Máquina**

Docente: Gustavo Adolfo Correa Solano, Arturo Ivan Villegas Andrade
Periodo: 2023-I

A continuación, se relaciona el informe de actividades de
investigación periodo 2023-1.

1.  Análisis computacional de la concentración de esfuerzos en muestras birrefringentes con esfuerzos residuales, basado en fotoelasticidad dinámica y técnicas de aprendizaje de máquina.

Se llevó a cabo el avance del proyecto en función del cronograma relacionado a continuación:

De acuerdo con el anterior cronograma se llevaron a cabo las actividades
del ítem 1 al 4 de la siguiente manera:

1.  Revisión del estado del arte: Se realizó la recopilación de la información importante relacionada. Ver anexo EDA_EF.docx.

2.  Generar secuencias de imágenes computacionales de fotoelasticidad digital, con y sin residuales:

Para el caso del disco bajo compresión diametral se generaron 8
secuencias. Inicialmente, las combinaciones fuente de iluminación –
sensor, fueron las dos seleccionadas en \[Fandiño et al., 2023\]. El
experimento simulado de aplicar carga incremental se modificó para
generar ahora secuencias de 320 imágenes, donde la carga diametral por
compresión variase desde 1 hasta 3200 Newtons, en incrementos de 10
Newtons. Como resultado, cada secuencia generada quedó con un total de
320 imágenes RGB. Se consideraron tres valores de carga para los
esfuerzos residuales: 320, 640 y 960 Newtons. Al considerar estas
consideraciones se obtuvo un total de 8 secuencias para el caso del
disco: una sin residuales, y tres más con residuales, por cada
combinación fuente – sensor. En la Figura 1 se muestran ejemplos de las
imágenes computacionales generadas para el caso del disco.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="media/image2.png"
style="width:1.1811in;height:1.1811in" /></td>
<td><img src="media/image3.png"
style="width:1.17964in;height:1.1811in" /></td>
<td><img src="media/image4.png"
style="width:1.17964in;height:1.1811in" /></td>
<td><img src="media/image5.png"
style="width:1.17964in;height:1.1811in" /></td>
<td><img src="media/image6.png"
style="width:1.17964in;height:1.1811in" /></td>
</tr>
<tr class="even">
<td><p><img src="media/image7.png"
style="width:1.17964in;height:1.1811in" /></p>
<p>Carga de 1 N, sin residuales.</p></td>
<td><p><img src="media/image8.png"
style="width:1.17964in;height:1.1811in" /></p>
<p>Carga: 3200 N, sin residuales.</p></td>
<td><p><img src="media/image9.png"
style="width:1.17964in;height:1.1811in" /></p>
<p>Carga de 3200 N + residual de 320 N.</p></td>
<td><p><img src="media/image10.png"
style="width:1.17964in;height:1.1811in" /></p>
<p>Carga de 3200 N + residual de 640 N.</p></td>
<td><p><img src="media/image11.png"
style="width:1.17964in;height:1.1811in" /></p>
<p>Carga de 3200 N + residual de 960 N.</p></td>
</tr>
</tbody>
</table>

**Figura 1.** Ejemplos de imágenes computacionales para las dos
combinaciones fuente de iluminación – sensor, consideradas en \[Fandiño
et al., 2023\]. Las imágenes superiores se obtuvieron con una
interacción espectral que incluye una fuente “Lásers”, mientras que las
imágenes inferiores se obtuvieron con una interacción que incluye una
fuente incandescente.

Las imágenes en la figura arriba corresponden a versiones a color del
modelo propuesto en \[Kihara, 2011\]. No obstante, se generaron también
imágenes para el modelo de la placa infinita bajo tensión biaxial, con
esfuerzos residuales, propuesto en \[Cárdenas, 1999\]. En la Figura 2 se
muestran imágenes obtenidas para este segundo modelo.

Las imágenes de las Figuras 1 y 2 tienen un tamaño de 513 x 513 píxeles,
se trabajaron en el espacio de color RGB, y se generaron utilizando
Matlab. Si bien el mismo trabajo puede hacerse en Python, se decidió
terminar la programación de los modelos en Matlab debido al
requerimiento de escribir con formato de imagen .png, los archivos que
se generaban al momento de simular la aplicación de carga incremental.
Debido a que eran muchas imágenes, se optó por generarlas en un equipo
local, con una versión local de Matlab, y no en COLAB de Google.

<img src="media/image12.png" style="width:2.3622in;height:2.3622in" />
<img src="media/image13.png" style="width:2.3622in;height:2.3622in" />

**(a) (b)**

<img src="media/image14.png" style="width:2.3622in;height:2.3622in" />
<img src="media/image15.png" style="width:2.3622in;height:2.3622in" />

**(c) (d)**

**Figura 2.** Ejemplos de imágenes computacionales para las dos
combinaciones fuente de iluminación – sensor, consideradas en \[Fandiño
et al., 2023\]. Modelo de la placa infinita bajo tensión biaxial
propuesto en \[Cárdenas, 1999\].

3.  ***<u>Programación - métodos basados en fotoelasticidad, para la
    evaluación del campo de esfuerzos:</u>***

 Se programó una versión para imágenes RGB, de la técnica para evaluar
 el campo de esfuerzos propuesta por Patterson y Wang. Esta técnica,
 conocida como *phase shifting technique* (PST), requiere 6 imágenes
 que, en un escenario real, se obtendrían a partir de igual número de
 configuraciones ópticas de un polariscopio circular. No obstante,
 estas imágenes fueron simuladas, ya que existen artículos donde se dan
 indicaciones de cómo obtener computacionalmente estas imágenes.

 Una vez programada la técnica PST, se compararon los mapas de esfuerzo
 obtenidos en las secuencias generadas con y sin esfuerzos residuales.
 En las siguientes figuras se muestran los resultados de dicha
 comparación.

 A modo de referencia, en la Figura 3 se muestran las superficies de
 esfuerzo analítica y reconstruidas mediante PST, para el caso del
 disco bajo compresión diametral sin presencia de esfuerzos residuales.
 En este caso se utilizó la combinación espectral que incluye la fuente
 de iluminación “Lásers”.

 <img src="media/image16.png" style="width:1.88056in;height:1.50713in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />

> \(a\)

 <img src="media/image17.png" style="width:1.88056in;height:1.50129in" />
 <img src="media/image18.png" style="width:1.88056in;height:1.50129in" />
 <img src="media/image19.png" style="width:1.84557in;height:1.50713in" />

> \(b\) (c) (d)

 **Figura 3.** Superficies de esfuerzo para el modelo del disco, sin
 esfuerzos residuales. La referencia es (a), que muestra la superficie
 de esfuerzos analítica, calculada mediante la Ley de Esfuerzo-Óptico.
 (b), (c) y (d) muestran reconstrucciones para los canales R, G y B,
 utilizando PST.

 A modo de referencia, en la Figura 4 se muestran las superficies de
 esfuerzo analítica y reconstruidas mediante PST, considerando esta vez
 la combinación espectral que incluye la fuente de iluminación
 incandescente.

 <img src="media/image20.png" style="width:1.88056in;height:1.50129in" />
 <img src="media/image21.png" style="width:1.88056in;height:1.50129in" />
 <img src="media/image22.png" style="width:1.84557in;height:1.47336in" />

> \(a\) (b) (c)

 **Figura 4.** Superficies de esfuerzo para el modelo del disco, sin
 esfuerzos residuales, calculada mediante la Ley de Esfuerzo-Óptico.
 (a), (b) y (c) muestran reconstrucciones para los canales R, G y B,
 respectivamente, utilizando PST.

 Ahora, en la Figura 5 se comparan las reconstrucciones del campo de
 esfuerzo, para el caso en que el disco tiene esfuerzos residuales de
 diferente magnitud.

 <img src="media/image20.png" style="width:1.88056in;height:1.50129in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />
 <img src="media/image21.png" style="width:1.88056in;height:1.50129in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />
 <img src="media/image22.png" style="width:1.84557in;height:1.47336in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />

> \(a\) (b) (c)

 <img src="media/image23.png" style="width:1.82356in;height:1.50129in" />
 <img src="media/image24.png" style="width:1.81624in;height:1.50129in" />
 <img src="media/image25.png" style="width:1.74124in;height:1.47336in" />

> \(a\) (b) (c)

 **Figura 5.** Superficies de esfuerzo para el modelo del disco, sin
 esfuerzos residuales y con esfuerzos residuales de 320 N, frente a una
 carga por compresión de 3200 N.

 En la Figura 5 puede verse una ligera disminución en la magnitud del
 máximo orden de franja, la cual se presume que es producida por la
 presencia de los esfuerzos residuales. La presencia de estos últimos
 se manifiesta visualmente por el aumento de los picos en la parte
 superior izquierda e inferior derecha, al lado de los puntos de
 aplicación de carga al disco. Vale la pena anotar que en este caso se
 consideró la combinación espectral que implica la fuente de
 iluminación “Lásers”.

 Estos picos que distorsionan la forma del campo de esfuerzos se ven
 más claramente cuando la magnitud de los esfuerzos residuales aumenta,
 como se observa en la Figura 6.

 <img src="media/image20.png" style="width:1.88056in;height:1.50129in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />
 <img src="media/image21.png" style="width:1.88056in;height:1.50129in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />
 <img src="media/image22.png" style="width:1.84557in;height:1.47336in"
 alt="Gráfico, Gráfico de superficie Descripción generada automáticamente" />

> \(a\) (b) (c)

 <img src="media/image26.png" style="width:1.82356in;height:1.45578in" />
 <img src="media/image27.png" style="width:1.81624in;height:1.44994in" />
 <img src="media/image28.png" style="width:1.74124in;height:1.42193in" />

> \(a\) (b) (c)

 **Figura 6.** Superficies de esfuerzo para el modelo del disco, sin
 esfuerzos residuales y con esfuerzos residuales de 960 N, frente a una
 carga por compresión de 3200 N.

4.  Desarrollo: metodología basada en aprendizaje de máquina para reconocer niveles de concentración de esfuerzo:

Se llevaron a cabo los siguientes experimentos:

**Experimento 1:** En las secuencias de imágenes se compararon las
últimas imágenes, para los casos de ausencia y presencia de esfuerzos
residuales. Esta comparación se hizo empleando la métrica índice de
similitud estructural (en Inglés Structural Similarity Index -SSIM)
\[Wang et al., 2004\]. La Figura 4 muestra ejemplos de la imagen final
sin y con esfuerzos residuales, para las dos combinaciones
fuente-sensor, considerando el disco bajo compresión diametral.

<img src="media/image29.png" style="width:1.5748in;height:1.57676in"
alt="Logotipo Descripción generada automáticamente con confianza media" /><img src="media/image30.png" style="width:1.5748in;height:1.57675in" />
<img src="media/image31.png" style="width:2.49066in;height:2.11596in" />

> \(a\) (b) (c)

<img src="media/image32.png" style="width:1.5748in;height:1.57675in" /><img src="media/image33.png" style="width:1.5748in;height:1.57675in" /><img src="media/image34.png" style="width:2.57944in;height:2.19843in" />

> \(d\) (e) (f)

 **Figura 4.** (a) y (d) imagen final en una secuencia, para carga
 final de 3200 N; sin esfuerzos residuales; (b) y (e) imagen final en
 la secuencia, para carga final de 3200 N; con esfuerzos residuales de
 960 N. (c) y (f) mapa con valores SSIM entre las imágenes izquierda y
 central.

 La imagen a la derecha se puede interpretar como el resultado de
 comparar las imágenes a la izquierda y en el centro de la Figura 4. De
 forma general, las regiones de color oscuro denotan diferencias entre
 tales imágenes. En términos del experimento simulado, la presencia de
 los esfuerzos residuales afectó una parte considerable de la geometría
 del disco. Las zonas más afectadas son aquellas cercanas que se ven de
 color oscuro en el mapa a la derecha: algunas cercanas a los puntos de
 aplicación de carga, pero también en zonas hacia los lados del disco.
 Las zonas menos afectadas son aquellas más alejadas de los puntos de
 aplicación de carga, y hacia el centro del disco, las cuales se ven de
 color blanco que están encerradas por la geometría del disco, como
 denota el contorno de color magenta.

 Las imágenes (c) y (f) de la Figura 4 fueron binarizadas, utilizando
 el método de Otsu \[Otsu, 1979\]. Los resultados de esta umbralización
 se muestran en las imágenes (a) y (d), respectivamente, de la Figura
 5. En esas imágenes, las regiones de color blanco denotan las zonas
 que cambian en las secuencias de imágenes de patrones de franja de
 fotoelasticidad, por efecto de la presencia del residual.

 <img src="media/image35.png" style="width:1.5748in;height:1.35419in" />
 <img src="media/image36.png" style="width:1.5748in;height:1.35419in" />
 <img src="media/image37.png" style="width:1.5748in;height:1.35419in" />

> \(a\) F0 = 320 N. (b) F0 = 630 N. (c) F0 = 960 N.

 <img src="media/image38.png" style="width:1.5748in;height:1.35419in" />
 <img src="media/image39.png" style="width:1.5748in;height:1.35419in" />
 <img src="media/image40.png" style="width:1.5748in;height:1.35419in" />

> \(d\) F0 = 320 N. (e) F0 = 630 N. (f) F0 = 960 N.

 **Figura 5.** En color blanco, las zonas donde los patrones de franja
 en las imágenes finales de las secuencias de imágenes cambian por
 efecto de la presencia de los esfuerzos residuales presentes en el
 modelo. Las imágenes de arriba son para la fuente “Lásers”, mientras
 que las imágenes de la fila inferior se obtuvieron para una fuente
 incandescente \[Fandiño, 2023\]. F0 denota el valor de la carga
 residual.

 Como resultado del experimento 1, se obtuvo que la presencia de los
 esfuerzos residuales influye en una zona de área variable dentro de la
 geometría del modelo bajo carga. Esta zona aumenta en la medida en que
 la magnitud de los esfuerzos residuales aumenta, aunque su tamaño
 puede verse modificado por la combinación espectral fuente de
 iluminación – sensor.

 **Experimento 2**

 El segundo experimento consistió en hacer clustering con las
 trayectorias por canal de color, caracterizadas con los descriptores
 mencionados en el documento **Consolidado – descriptores.docx**. Los
 resultados más relevantes de estos experimentos se muestran en las
 Figuras a continuación.

 En la Figura 6 se muestran resultados del clustering para la secuencia
 de imágenes que incluye la fuente “Lásers”, siendo la carga de los
 esfuerzos residuales 320 N. La imagen a la derecha es la última imagen
 de la secuencia; la imagen central es la zona afectada por los
 esfuerzos residuales, y la imagen a la derecha, es la segmentación que
 resulta al agrupar zonas en la geometría del disco, que tienen
 evolución similar en sus valores de intensidad de píxel. Lo que indica
 la imagen (c) es que cuando el disco es sometido a carga incremental,
 pero además tiene esfuerzos residuales, es que en los alrededores de
 donde se aplican los esfuerzos residuales, hay un comportamiento que
 se parece al de una zona de menor concentración de esfuerzos. Esto se
 puede ver por la región que se observa de color azul oscuro. Las zonas
 de color rojo, denotan que hay una región en el disco donde aumenta
 considerablemente la concentración de esfuerzos, pero esta zona no
 sufre una afectación notoria por efecto de la presencia de los
 esfuerzos residuales. Esto se ve incluso en la imagen (b).

 <img src="media/image41.png" style="width:1.5748in;height:1.5748in" /><img src="media/image35.png" style="width:1.77165in;height:1.5748in" />
 <img src="media/image42.png" style="width:1.5748in;height:1.5748in" />

\(a\) (b) (c)

**Figura 6.** Resultados del clustering para secuencia de imágenes
generada considerando la fuente de iluminación “Lásers”. La imagen (a)
es la imagen final en la secuencia analizada; (b) muestra en color
blanco las zonas alteradas por la presencia de residuales, respecto a la
imagen con la misma carga, pero sin residuales; (c) es la segmentación
que se obtiene al agrupar la trayectorias por canal de color para los
descriptores de Haralick.

En la Figura 7 se muestran las segmentaciones con todos los grupos de
descriptores considerados en el proyecto. Se debe tener presente que las
imágenes evaluadas en esta figura se generaron con la interacción
espectral que incluía la fuente incandescente. De las imágenes (c) a
(e), el resultado más relevante se observa en (c), donde pueden verse
dos regiones de color magenta que son similares en tamaño a las regiones
de color rojo en la Figura 6.

Esto indica que la zona magenta corresponde a la región con mayor
concentración de esfuerzos para las imágenes analizadas. Ahora bien, se
observó que esta zona aparece deformada por la presencia de los
esfuerzos residuales. Esto indica que, para esta interacción espectral,
la evolución de las intensidades de píxel que tal, que el efecto de los
esfuerzos residuales fue comparable al efecto producido por la
aplicación de carga por compresión; por lo menos comparando este
resultado con la anterior interacción espectral.

Otro resultado de esta figura es que el grupo de descriptores Sampled
signal tuvo un mejor desempeño reconociendo zonas con diferente
concentración de esfuerzos, en el experimento simulado. Y en general,
que los descriptores tienen un desempeño variable, el cual es función de
la interacción espectral utilizada para generar las imágenes con
patrones de franja de fotoelasticidad.

 <img src="media/image43.png" style="width:1.37795in;height:1.37402in" />

> \(a\)

 <img src="media/image38.png" style="width:1.5748in;height:1.35419in" /><img src="media/image44.png" style="width:1.37795in;height:1.37402in" /><img src="media/image45.png" style="width:1.37795in;height:1.37402in" /><img src="media/image46.png" style="width:1.37795in;height:1.37402in" />

> \(b\) (c) Sampled signal. (d) Haralick. (e) cspc.

 **Figura 7.** Resultados del clustering para secuencia de imágenes
 generada considerando la fuente de iluminación incandescente. La
 imagen (a) es la imagen final en la secuencia analizada; (b) muestra
 en color blanco las zonas alteradas por la presencia de residuales,
 respecto a la imagen con la misma carga pero sin residuales; (c), (d)
 y (e) muestran los resultados del clustering según el grupo de
 descriptores considerados.

**Referencias**

\[Fandiño et al., 2023\] Fandiño-Toro, H., Aristizábal-López, Y.,
Restrepo-Martínez, A., & Briñez-de León, J. (2023). Fringe pattern
analysis to evaluate light sources and sensors in digital
photoelasticity. *Applied Optics*, *62*(10), 2560-2568.

\[Otsu, 1979\] Otsu, N. (1979). A threshold selection method from
gray-level histograms. *IEEE transactions on systems, man, and
cybernetics*, *9*(1), 62-66.

\[Wang et al., 2004\] Wang, Z., Bovik, A. C., Sheikh, H. R., &
Simoncelli, E. P. (2004). Image quality assessment: from error
visibility to structural similarity. IEEE transactions on image
processing, 13(4), 600-612.
